﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meat : MonoBehaviour {

    public int healthValue;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            BeatNUpController play = other.gameObject.GetComponent<BeatNUpController>();
            play.AddHealth(healthValue);
            Destroy(gameObject);
        }
    }

}
