﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    public GameObject[] item_prefab;
    public bool isDestroyed;
    public Renderer rend;

    BlinkEffect blinkEffect;
    Collider coll;
    Rigidbody rb;

    private void Start()
    {
        coll = GetComponent<Collider>();
        blinkEffect = GetComponent<BlinkEffect>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (isDestroyed)
        {
            rend.enabled = blinkEffect.isVisible;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isDestroyed)
        {
            DropItem();
            Destroy(gameObject, 3);
            isDestroyed = true;
            coll.isTrigger = true;
            rb.isKinematic = true;
        }
    }

    void DropItem()
    {
        int randNumber = Random.Range(0, item_prefab.Length);
        Instantiate(item_prefab[randNumber],transform.position, Quaternion.identity);
    }
}
