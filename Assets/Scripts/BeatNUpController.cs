﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatNUpController : MonoBehaviour {

    public int live;
    public int str;
    public int health;
    public float vel;
    public float jump;
    public int score;
    //public float gravity = -10;
    public bool isAlive;
    public bool isGround;
    public bool isPunch;
    public bool isJump;
    public Transform groundCheck;
    public Transform punchCheck;
    public LayerMask lm_solid;

    public EnemyController lastEnemy;

    float velx, velz;
    Rigidbody rb;
    Ray ray;
    RaycastHit rayHit;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        velx = Input.GetAxisRaw("Horizontal");
        velz = Input.GetAxisRaw("Vertical");

        isGround = Physics.Raycast(transform.position, -Vector3.up, out rayHit,(transform.position.y - groundCheck.position.y), lm_solid);
        Debug.DrawRay(transform.position,-Vector3.up * (transform.position.y - groundCheck.position.y),Color.yellow);

        if(Physics.BoxCast(transform.position, punchCheck.transform.localScale, transform.right, out rayHit, Quaternion.identity,1)){
            if(rayHit.collider.gameObject.tag == "Enemy")
            {
                isPunch = true;
            }
        }

        Debug.DrawRay(transform.position, transform.right, Color.yellow);

        if (Input.GetKeyDown(KeyCode.J) && isGround)
        {
            isJump = true;
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            //realizar um soco;
            //isPunch = true;
        }

    }

    void FixedUpdate()
    {
        Walk();
        Jump();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //diminuir vida pela força de atk do inimigo
            lastEnemy = collision.gameObject.GetComponent<EnemyController>();
            if (lastEnemy.CheckIsAlive() == false)
            {
                score += 10;
                lastEnemy.enemyAction = EnemyController.ENEMY_ACTION.DIE;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Enemy")
        {
            //diminuir vida pela força de atk do inimigo
            //CheckIsAlive();
            //Destroy(other.gameObject);
            //score += 10;
        }
    }

    //void CallGravity()
    //{
    //    //rb.velocity = new Vector3(rb.velocity.x, gravity, rb.velocity.z);
    //    rb.AddForce(Vector3.up * gravity, ForceMode.VelocityChange);
    //}

    public EnemyController GetLastEnemy()
    {
        return lastEnemy;
    }

    public bool CheckIsAlive()
    {
        if (health > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void Walk()
    {
        rb.velocity = new Vector3(velx * vel, rb.velocity.y, velz * vel);
    }

    void Jump()
    {
        if (isJump)
        {
            rb.AddForce(Vector3.up * jump,ForceMode.VelocityChange);
            isJump = false;
            //Debug.Log("Jump");
        }
    }

    void Run()
    {

    }

    void Atk()
    {

    }

    void Special()
    {

    }

    public void AddHealth(int value)
    {
        health += value;
    }

}
