﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelect : MonoBehaviour {

    public Image[] imgBorder;
    public int indexSelected;

	// Use this for initialization
	void Start () {
        imgBorder[indexSelected].enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.D))
        {
            imgBorder[indexSelected].enabled = false;
            AddIndex();
            imgBorder[indexSelected].enabled = true;
            //ShowSelectedPlayer();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            imgBorder[indexSelected].enabled = false;
            SubIndex();
            imgBorder[indexSelected].enabled = true;
            //ShowSelectedPlayer();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.instance.LoadLevel(2);
            Debug.Log("SelectedPlayer - "+(indexSelected+1));
        }

    }

    //void ShowSelectedPlayer()
    //{
    //    foreach (Image item in imgBorder)
    //    {
    //        item.enabled = false;
    //    }

    //    imgBorder[indexSelected].enabled = true;
    //}

    void AddIndex()
    {
        if (indexSelected < 3)
        {
            indexSelected++;
        }
    }

    void SubIndex()
    {
        if (indexSelected > 0)
        {
            indexSelected--;
        }
    }
}
