﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public Sprite enemyImg;
    public string enemyName;

    public int str;
    public int health;
    public float vel;
    public float jump;

    public enum  ENEMY_ACTION{ NOTHING, WALK, JUMP, DIE };
    public ENEMY_ACTION enemyAction;

    Rigidbody rb;
    float velx, velz;

    public bool isAlive;
    public bool isGround;
    public bool isJump;
    public Transform groundCheck;
    public LayerMask lm_solid;
    Ray ray;
    RaycastHit rayHit;
    //UIController uictrl;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

        if(enemyAction == ENEMY_ACTION.NOTHING)
        {
            //target 
        }

		if(enemyAction == ENEMY_ACTION.WALK)
        {
            Walk();
        }

        if(enemyAction == ENEMY_ACTION.JUMP)
        {
            Jump();
            enemyAction = ENEMY_ACTION.NOTHING;
        }

        if(enemyAction == ENEMY_ACTION.DIE)
        {
            Destroy(gameObject,0.2f);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        
    }

    public bool CheckIsAlive()
    {
        if (health > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void Walk()
    {
        rb.velocity = new Vector3(velx * vel, rb.velocity.y, velz * vel);
    }

    void Jump()
    {
        if (isJump)
        {
            rb.AddForce(Vector3.up * jump, ForceMode.VelocityChange);
            isJump = false;
            //Debug.Log("Jump");
        }
    }
}
