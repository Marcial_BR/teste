﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroScreen : MonoBehaviour {

    public Text txtTitle;
    public Text txtSubTitle;
    public Text txtKeyMessage;
    public Image imgBackground;

    BlinkEffect beStart;

    // Use this for initialization
    void Start () {

        beStart = txtKeyMessage.GetComponent<BlinkEffect>();
	}
	
	// Update is called once per frame
	void Update () {

        txtKeyMessage.enabled = beStart.isVisible;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.instance.LoadLevel(1);
        }
	}
}
