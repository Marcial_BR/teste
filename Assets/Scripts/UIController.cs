﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public Text txtPlayerName;
    public Text txtEnemyName;
    public Text txtScore;
    public Text txtPlayerNumber;
    public Text txtPressStart;

    public Image imgPlayer;
    public Image imgEnemy;

    public GameObject enemyPainel;

    //public BlinkEffect be_start;
    BlinkEffect be_start;

    // Use this for initialization
    void Start () {
        be_start = txtPressStart.GetComponent<BlinkEffect>();
	}

    void Update()
    {
        txtPressStart.enabled =  be_start.isVisible;
    }

    public void SetAllPlayer(int score, string name, Sprite spr)
    {
        SetScore(score);
        SetPlayerName(name);
        SetPlayerImage(spr);
    }

    public void SetAllEnemy(string name)
    {
        txtEnemyName.text = name;
        //imgEnemy.overrideSprite = spr;
    }

    public void SetPlayerName(string naem)
    {
        txtPlayerName.text = name;
    }

    public void SetEnemyName(string name)
    {
        txtEnemyName.text = name;
    }

    public void SetScore(int score)
    {
        txtScore.text = score.ToString();
    }

    public void SetPlayerImage(Sprite spr)
    {
        imgPlayer.overrideSprite = spr;
    }

    public void SetEnemyImage(Sprite spr)
    {
        imgEnemy.overrideSprite = spr;
    }

    public void ShowEnemyPainel()
    {
        enemyPainel.SetActive(true);
    }

    public void HideEnemyPainel()
    {
        enemyPainel.SetActive(false);
    }

}
