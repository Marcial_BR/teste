﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemy : MonoBehaviour {

	public string myName;
	public Sprite myPhoto;
	public float speed = 2, health = 10, strong = 5;

	private Rigidbody R3D;
	private Transform player;
	private UIController uIController;
	private Animator anim;
	private bool isDead, isWalk = true, attack = true;
	private float distance, walkTime, vertical;

	private void Start () {
		player = GameObject.FindWithTag("Player").transform;
		anim = GetComponent<Animator>();
		R3D = GetComponent<Rigidbody>();
	}
	
	private void Update () {
		if(isDead)
			return;
			
		transform.LookAt(player);
		Vector3 distance = player.position - transform.position;
		float horizontal = distance.x / Mathf.Abs(distance.x);
		if(Mathf.Abs(distance.x) < 1.2f)
				horizontal = 0;

		walkTime += Time.deltaTime;
		if(walkTime >= Random.Range(1f, 2f)){
				vertical = Random.Range(-1, 2);
				walkTime = 0;
		}

		if(horizontal == 0){
			isWalk = false;
			if(vertical != 0)
				isWalk = true;
		}  else 
				isWalk = true;

		// if(Mathf.Abs(targetDistance.x) < 1.2f && Mathf.Abs(targetDistance.z) < 1.2f && Time.time > m_nextAttack){
		// 	m_animator.SetTrigger("Attack");
		// 	m_speed = 0;
		// 	m_nextAttack = Time.time + m_attackRate;
		// }

		R3D.velocity = new Vector3(horizontal * speed, R3D.velocity.y, vertical * speed);
		anim.SetBool("Walk", isWalk);
	}

	public void ApplyDamage(int _hit){
		health  -= _hit;
		//uIController.updateEnemy(myName, myPhoto, health);
		if(health <=0){
			isDead = true;
			speed = 0;
		}
	}
}